package com.deepra.ic.ws;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

@Path("ContactUs")
public class ContactWS {

    private static final String HOST = "localhost";
    private static final int PORTNO = 27017;
    private static final String DATABASE = "ideascentregroup";
    private static final String ContactCollection = "Contact";
    private static final String NAME = "Name";
    private static final String EMAIL = "Email";
    private static final String MESSAGE = "Message";
    private static final String Success = "Mail Sent Successfully";
    private static final String Fail = "Failed to Contact";
    static final long ONE_MINUTE_IN_MILLIS = 60000;
    private static final String ContactWSPath = "ContactWSPath";

    @POST
    @Path(ContactWSPath)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)

    @Produces("application/json")
    public Response Register(@FormParam(NAME) String Name, @FormParam(EMAIL) String Email,
            @FormParam(MESSAGE) String Message) throws Exception {
        // String uri="";
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);
        DBCollection register = db.getCollection(ContactCollection);
        try {

            BasicDBObject obj = new BasicDBObject();

            System.out.println(Name);
            System.out.println(Email);
            System.out.println(Message);

            obj.append(NAME, Name);
            obj.append(EMAIL, Email);
            obj.append(MESSAGE, Message);

            WriteResult result = register.insert(obj);

            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(NAME, Name);
            jsonObject.put(EMAIL, Email);
            jsonObject.put(MESSAGE, Message);
            jsonObject.put("Status", Success);
            array.put(jsonObject);
            String Successful = array.toString();

            return Response.status(200).entity(Successful).build();
        } catch (Exception e) {

            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("Name", Name);
            jsonObj.put("Email", Email);
            jsonObj.put("Message", Message);
            jsonObj.put("Status", Fail);
            arr.put(jsonObj);
            String Fail = arr.toString();

            return Response.status(400).entity(Fail).build();
        } finally {
            mongoClient.close();
        }
    }
}
