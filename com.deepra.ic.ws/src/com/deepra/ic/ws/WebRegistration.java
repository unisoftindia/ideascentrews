package com.deepra.ic.ws;

import java.net.URI;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

@Path("AdminRegistration")
public class WebRegistration {

    public static final String HOST = "localhost";
    public static final int PORTNO = 27017;
    public static final String DATABASE = "ideascentregroup";
    public static final String COLLECTIONREGISTER = "AdminRegister";
    public static final String ADMINCOLLECTION = "AdminDb";
    static final long ONE_MINUTE_IN_MILLIS = 60000;
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String REGISTRATION_WS_PATH = "/AddUser";
    public static final String PRODUCES_TEXT_HTML = "text/html";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String SET = "$set";
    public static final String RegistrationSuccess = "Successfully Registered";
    public static final String RegistrationFailed = "Failed to Register";
    public static final String UNIQUE = "unique";
    public static final String LOGIN_WS_PATH = "/Login";
    public static final String GETEMAIl = "GetEmail";
    public static final String WEBAPI = "com.deepra.ic.ws/ideascentre";
    public static final String TOKEN = "token";
    public static final String DASHBOARD = "/Dashboard.html";
    public static final String USERTYPE = "UserType";
    public static final String LOGIN_FAIL = "/Login.html";

    private static final String MESSAGE_DIGEST = "SHA-256";

    @POST
    @Path(REGISTRATION_WS_PATH)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response RegisterAdmin(@FormParam(NAME) String name, @FormParam(EMAIL) String email,
            @FormParam(PASSWORD) String password, @FormParam(USERTYPE) String UserType) throws Exception {
        // String uri="";
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);
        DBCollection register = db.getCollection(COLLECTIONREGISTER);
        try {
            BasicDBObject keys = new BasicDBObject(EMAIL, 1);
            BasicDBObject options = new BasicDBObject(UNIQUE, TRUE);
            register.createIndex(keys, options);
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST);
            System.out.println("Name: " + name);
            System.out.println("Email: " + email);
            System.out.println("Password: " + password);
            md.update(password.getBytes()); // Change this to "UTF-16" if needed
            String digest = bytesToHex(md.digest());
            BasicDBObject obj = new BasicDBObject();

            obj.append(NAME, name);
            obj.append(EMAIL, email);
            obj.append(PASSWORD, digest);
            obj.append(USERTYPE, UserType);
            System.out.println("en: " + digest);

            WriteResult result = register.insert(obj);

            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", name);
            jsonObject.put("Email", email);
            jsonObject.put("Message", RegistrationSuccess);
            array.put(jsonObject);
            String RegistrationSuccess = array.toString();

            // Response.status(200).entity(result).build();
            // uri = uri + "../Login.html";
            // ResponseBuilder response = Response.temporaryRedirect(new
            // URI(uri));
            return Response.status(200).entity(RegistrationSuccess).build();
        } catch (Exception e) {

            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("Name", name);
            jsonObj.put("Email", email);
            jsonObj.put("Message", RegistrationFailed);
            arr.put(jsonObj);
            String RegistrationFail = arr.toString();
            // uri = uri + "../Registration-Error.html";
            // ResponseBuilder response = Response.temporaryRedirect(new
            // URI(uri));
            return Response.status(400).entity(RegistrationFail).build();
        } finally {
            mongoClient.close();
        }
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes)
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    @Context
    UriInfo uri;
    @Context
    private HttpServletResponse response;
    @Context
    private HttpServletRequest request;

    @POST
    @Path(LOGIN_WS_PATH)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(PRODUCES_TEXT_HTML)
    public Response authenticateUser(@FormParam(EMAIL) String email, @FormParam(PASSWORD) String password)
            throws Exception {
        String uri2 = "";
        try {
            // Authenticate the user using the credentials provided
            boolean authenticate = authenticate(email, password);

            if (authenticate == true) {
                HttpSession session = request.getSession(true);
                session.setMaxInactiveInterval(20 * 60);
                String token = issueToken(email);
                String value = token;
                System.out.println(value);
                String URI = uri.getBaseUri().toString();
                String newURI = URI.replace(WEBAPI, "com.deepra.ic.ws");
                System.out.println(newURI);
                UriBuilder builder = UriBuilder.fromPath(newURI).path(DASHBOARD).queryParam(TOKEN, token);
                URI uri = builder.build();
                return Response.seeOther(uri).header("token", token).build();
            }
        } catch (Exception e) {

            String URI = uri.getBaseUri().toString();
            String newURI = URI.replace(WEBAPI, "com.deepra.ic.ws");
            System.out.println(newURI);
            UriBuilder builder = UriBuilder.fromPath(newURI).path(LOGIN_FAIL);
            URI uri = builder.build();
            return Response.seeOther(uri).build();

        }

        String URI = uri.getBaseUri().toString();
        String newURI = URI.replace(WEBAPI, "ideascentre");
        System.out.println(newURI);
        UriBuilder builder = UriBuilder.fromPath(newURI).path(LOGIN_FAIL);
        URI uri = builder.build();
        return Response.seeOther(uri).build();

    }

    private String issueToken(String email) throws UnknownHostException {

        Random random = new SecureRandom();
        String authToken = UUID.randomUUID().toString();
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);

        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);

        DBCollection register = db.getCollection(COLLECTIONREGISTER);
        DBObject query = new BasicDBObject(EMAIL, email);
        DBObject update = new BasicDBObject();
        update.put(SET, new BasicDBObject(TOKEN, authToken));
        WriteResult result = register.update(query, update);

        mongoClient.close();
        return authToken;
    }

    private boolean authenticate(String email, String password) throws Exception {
        // To connect to mongodb server
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);

        DBCollection register = db.getCollection(ADMINCOLLECTION);
        DBObject query = new BasicDBObject(EMAIL, email);
        try {
            // resultant document fetched by satisfying the criteria
            DBObject dbo = register.findOne(query);
            String encryptedDBPassword = dbo.get(PASSWORD).toString();
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST);
            md.update(password.getBytes()); // Change this to "UTF-16" if needed
            String newDigest = bytesToHex(md.digest());
            if ((encryptedDBPassword).equals(newDigest)) {
                return true;
            } else {
                return false;
            }
        } finally {
            mongoClient.close();
        }
    }

}
