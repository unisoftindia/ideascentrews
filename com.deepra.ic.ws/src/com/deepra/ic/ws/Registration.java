package com.deepra.ic.ws;

import javax.ws.rs.Path;

import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import java.util.Random;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

@Path("Registration")
public class Registration {

    public static final String HOST = "localhost";
    public static final int PORTNO = 27017;
    public static final String DATABASE = "ideascentregroup";
    public static final String REGISTERCOLLECTION = "Register";
    public static final String ADMINCOLLECTION = "AdminDb";
    static final long ONE_MINUTE_IN_MILLIS = 60000;
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String USERTYPE = "UserType";
    public static final String RegistrationSuccess = "Successfully Registered";
    public static final String RegistrationFailed = "Failed to Register";
    private static final String UNIQUE = "unique";
    private static final String RegisterWSPath = "Register";
    private static final String AdminRegisterWSPath = "AdminRegister";
    private static final String GetAllUsers = "GetAllUsers";
    private static final String UpdateUserInfo = "UpdateUserInfo";
    private static final String GetUser = "GetUser";
    private static final String TRUE = "true";
    private static final String MESSAGE_DIGEST = "SHA-256";
    private static final String SET = "$set";
    private static final String UpdateSuccess = "Successfully Updated User Information";
    private static final String UpdateFail = "Faild to Update User Information";

    @POST
    @Path(AdminRegisterWSPath)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces("text/html")
    @Produces("application/json")
    public Response AdminRegister(@FormParam(NAME) String name, @FormParam(EMAIL) String email,
            @FormParam(PASSWORD) String password, @FormParam(USERTYPE) String UserType) throws Exception {
        // String uri="";
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);
        DBCollection register = db.getCollection(ADMINCOLLECTION);
        try {
            BasicDBObject keys = new BasicDBObject(EMAIL, 1);
            BasicDBObject options = new BasicDBObject(UNIQUE, TRUE);
            register.createIndex(keys, options);
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST);
            System.out.println(password);
            md.update(password.getBytes()); // Change this to "UTF-16" if needed
            String digest = bytesToHex(md.digest());
            BasicDBObject obj = new BasicDBObject();

            System.out.println(email);
            System.out.println(password);
            System.out.println(UserType);
            System.out.println(name);

            obj.append(NAME, name);
            obj.append(EMAIL, email);
            obj.append(PASSWORD, digest);
            obj.append(USERTYPE, UserType);
            System.out.println("en: " + digest);

            WriteResult result = register.insert(obj);

            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", name);
            jsonObject.put("Email", email);
            jsonObject.put("Message", RegistrationSuccess);
            array.put(jsonObject);
            String RegistrationSuccess = array.toString();

            // Response.status(200).entity(result).build();
            // uri = uri + "../Login.html";
            // ResponseBuilder response = Response.temporaryRedirect(new
            // URI(uri));
            return Response.status(200).entity(RegistrationSuccess).build();
        } catch (Exception e) {

            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("Name", name);
            jsonObj.put("Email", email);
            jsonObj.put("Message", RegistrationFailed);
            arr.put(jsonObj);
            String RegistrationFail = arr.toString();
            // uri = uri + "../Registration-Error.html";
            // ResponseBuilder response = Response.temporaryRedirect(new
            // URI(uri));
            return Response.status(400).entity(RegistrationFail).build();
        } finally {
            mongoClient.close();
        }
    }

    @POST
    @Path(RegisterWSPath)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    // @Produces("text/html")
    @Produces("application/json")
    public Response Register(@FormParam(NAME) String name, @FormParam(EMAIL) String email,
            @FormParam(PASSWORD) String password, @FormParam(USERTYPE) String UserType) throws Exception {
        // String uri="";
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);
        DBCollection register = db.getCollection(REGISTERCOLLECTION);
        try {
            BasicDBObject keys = new BasicDBObject(EMAIL, 1);
            BasicDBObject options = new BasicDBObject(UNIQUE, TRUE);
            register.createIndex(keys, options);
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST);
            System.out.println(password);
            md.update(password.getBytes()); // Change this to "UTF-16" if needed
            String digest = bytesToHex(md.digest());
            BasicDBObject obj = new BasicDBObject();

            System.out.println(email);
            System.out.println(password);
            System.out.println(UserType);
            System.out.println(name);

            obj.append(NAME, name);
            obj.append(EMAIL, email);
            obj.append(PASSWORD, digest);
            obj.append(USERTYPE, UserType);
            System.out.println("en: " + digest);

            WriteResult result = register.insert(obj);

            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", name);
            jsonObject.put("Email", email);
            jsonObject.put("Message", RegistrationSuccess);
            array.put(jsonObject);
            String RegistrationSuccess = array.toString();

            // Response.status(200).entity(result).build();
            // uri = uri + "../Login.html";
            // ResponseBuilder response = Response.temporaryRedirect(new
            // URI(uri));
            return Response.status(200).entity(RegistrationSuccess).build();
        } catch (Exception e) {

            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("Name", name);
            jsonObj.put("Email", email);
            jsonObj.put("Message", RegistrationFailed);
            arr.put(jsonObj);
            String RegistrationFail = arr.toString();
            // uri = uri + "../Registration-Error.html";
            // ResponseBuilder response = Response.temporaryRedirect(new
            // URI(uri));
            return Response.status(400).entity(RegistrationFail).build();
        } finally {
            mongoClient.close();
        }
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes)
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    @GET
    @Path(GetAllUsers)
    @Produces(MediaType.APPLICATION_JSON)
    public String getVideo() throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(REGISTERCOLLECTION);

        DBCursor cursor = coll.find();
        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                list.add(o.toString());
            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

    @GET
    @Path(GetUser)
    @Produces(MediaType.APPLICATION_JSON)
    public String getVideo(@QueryParam(EMAIL) String email) throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(REGISTERCOLLECTION);
        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put(EMAIL, email);
        DBCursor cursor = coll.find(whereQuery);
        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                list.add(o.toString());
            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

    @POST
    @Path(UpdateUserInfo)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response updateUser(@FormParam(EMAIL) String email, @FormParam(NAME) String name,
            @FormParam(USERTYPE) String UserType) throws Exception {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        // Now connect to your databases DB db = mongoClient.getDB(DATABASE);

        DBCollection register = db.getCollection(REGISTERCOLLECTION);
        DBObject query = new BasicDBObject(EMAIL, email);
        DBObject update = new BasicDBObject();
        try {

            update.put(SET, new BasicDBObject(USERTYPE, UserType));
            WriteResult result = register.update(query, update);
            mongoClient.close();
            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Name", name);
            jsonObject.put("Email", email);
            jsonObject.put("UserType", UserType);
            jsonObject.put("Message", UpdateSuccess);
            array.put(jsonObject);
            String Success = array.toString();
            return Response.status(200).entity(Success).build();

        } catch (Exception e) {
            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("Name", name);
            jsonObj.put("Email", email);
            jsonObj.put("UserType", UserType);
            jsonObj.put("Message", UpdateFail);
            arr.put(jsonObj);
            String Success = arr.toString();
            return Response.status(200).entity(Success).build();

        } finally {
            mongoClient.close();
        }

    }
}
