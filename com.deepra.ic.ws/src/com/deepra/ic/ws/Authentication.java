package com.deepra.ic.ws;

import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

@Path("/Authentication")
public class Authentication {

    public static final String HOST = "localhost";
    public static final int PORTNO = 27017;
    public static final String DATABASE = "ideascentregroup";
    public static final String REGISTERCOLLECTION = "Register";
    public static final String LoginWS = "Login";
    public static final String EMAIL = "email";
    public static final String SUCCESS = "Success";
    public static final String FAIL = "Failed To Login";
    public static final String PASSWORD = "password";
    static final long ONE_MINUTE_IN_MILLIS = 60000;
    private static final String MESSAGE_DIGEST = "SHA-256";
    // private static final String Success="User Valid";
    private static final String MESSAGE = "Message";

    @Context
    private HttpServletResponse response;
    @Context
    private HttpServletRequest request;

    @POST
    @Path(LoginWS)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("text/html")
    public Response authenticateUser(@FormParam(EMAIL) String email, @FormParam(PASSWORD) String password)
            throws Exception {
        try {

            boolean authenticate = authenticate(email, password);

            if (authenticate == true) {
                HttpSession session = request.getSession(true);
                session.setAttribute(EMAIL, email);
                session.setMaxInactiveInterval(20 * 60);

                return Response.status(200).entity(UserInfo(email)).build();
            }

        } catch (Exception e) {
            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(EMAIL, email);
            jsonObject.put(MESSAGE, FAIL);
            array.put(jsonObject);
            String Failed = array.toString();
            return Response.status(401).entity(Failed).build();
        }
        JSONArray array = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(EMAIL, email);
        jsonObject.put(MESSAGE, FAIL);
        array.put(jsonObject);
        String Failed = array.toString();
        return Response.status(401).entity(Failed).build();
    }

    private boolean authenticate(String email, String password) throws Exception {
        // To connect to mongodb server
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);
        DBCollection register = db.getCollection(REGISTERCOLLECTION);
        DBObject query = new BasicDBObject(EMAIL, email);
        try {
            // resultant document fetched by satisfying the criteria
            DBObject dbo = register.findOne(query);
            String encryptedDBPassword = dbo.get(PASSWORD).toString();
            System.out.println("en: " + encryptedDBPassword);
            MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST);
            System.out.println("pw: " + password);
            md.update(password.getBytes()); // Change this to "UTF-16" if needed
            String newDigest = bytesToHex(md.digest());

            System.out.println("de: " + newDigest);
            if ((encryptedDBPassword).equals(newDigest)) {
                return true;
            } else {
                return false;
            }
        } finally {
            mongoClient.close();
        }
    }

    public static String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte byt : bytes)
            result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1));
        return result.toString();
    }

    public static String UserInfo(String email) throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(REGISTERCOLLECTION);
        BasicDBObject whereQuery = new BasicDBObject();
        System.out.println("Email Parameter :" + email);
        whereQuery.put("email", email);
        BasicDBObject select = new BasicDBObject();
        select.put("name", 1);
        select.put("email", 1);
        select.put("UserType", 1);
        DBCursor cursor = coll.find(whereQuery, select);

        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();

                list.add(o.toString());

            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

}
