package com.deepra.ic.ws;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

@Path("Videos")
public class VideoWS {

    private static final String HOST = "localhost";
    private static final int PORTNO = 27017;
    private static final String DATABASE = "ideascentregroup";
    private static final String TRAININGCOLLECTION = "TrainingVideos";
    private static final String INTROCOLLECTION = "IntroVideos";
    static final long ONE_MINUTE_IN_MILLIS = 60000;

    private static final String VideoPath = "VideoPath";
    private static final String VideoTitle = "VideoTitle";
    private static final String VideoDescription = "VideoDescription";
    private static final String VideoType = "VideoType";
    private static final String TRUE = "true";
    private static final String VideoSuccess = "Video Successfully Added";
    private static final String VideoFailed = "Failed to Add Video";
    private static final String UNIQUE = "unique";
    private static final String AddTrainigVideoPath = "AddTrainingVideos";
    private static final String AddIntroVideoPath = "AddIntroVideos";
    private static final String GetAllTrainigVideoPath = "GetAllTrainingVideos";
    private static final String GetTrainigVideo = "GetTrainingVideos";
    private static final String GetIntroVideo = "GetIntroVideos";
    private static final String GetAllIntroVideo = "GetAllIntroVideos";
    private static final String VideoCategory = "VideoCategory";
    private static final String UpdateTrainingVideoInfo = "UpdateTrainingVideoInfo";
    private static final String UpdateIntroVideoInfo = "UpdateIntroInfo";
    private static final String SET = "$set";
    private static final String UpdateSuccess = "Successfully Updated Video Information";
    private static final String UpdateFail = "Faild to Update Video Information";

    @POST
    @Path(AddTrainigVideoPath)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response AddVideo(@FormParam(VideoWS.VideoTitle) String VideoTitle, @FormParam(VideoWS.VideoPath) String VideoPath,
            @FormParam(VideoWS.VideoDescription) String VideoDescription, @FormParam(VideoWS.VideoType) String VideoType)
            throws Exception {

        MongoClient mongoClient = new MongoClient(HOST, PORTNO);

        DB db = mongoClient.getDB(DATABASE);
        DBCollection videos = db.getCollection(TRAININGCOLLECTION);
        try {
            BasicDBObject keys = new BasicDBObject("VideoTitle", 1);
            BasicDBObject options = new BasicDBObject(UNIQUE, TRUE);
            videos.createIndex(keys, options);

            System.out.println(VideoPath);
            System.out.println(VideoTitle);
            System.out.println(VideoDescription);
            System.out.println(VideoType);

            BasicDBObject obj = new BasicDBObject();

            obj.append("VideoTitle", VideoTitle);
            obj.append("VideoPath", VideoPath);
            obj.append("VideoDescription", VideoDescription);

            obj.append("VideoType", VideoType);

            WriteResult result = videos.insert(obj);

            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("VideoTitle", VideoTitle);
            jsonObject.put("VideoPath", VideoPath);
            jsonObject.put("VideoDescription", VideoDescription);
            jsonObject.put("VideoType", VideoType);

            jsonObject.put("Message", VideoSuccess);
            array.put(jsonObject);
            String Success = array.toString();
            return Response.status(200).entity(Success).build();
        } catch (Exception e) {
            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();

            jsonObj.put("VideoPath", VideoPath);
            jsonObj.put("VideoTitle", VideoTitle);
            jsonObj.put("VideoDescription", VideoDescription);

            jsonObj.put("VideoType", VideoType);
            jsonObj.put("Message", VideoFailed);
            arr.put(jsonObj);
            String Fail = arr.toString();
            return Response.status(400).entity(Fail).build();
        } finally {
            mongoClient.close();
        }

    }

    @POST
    @Path(AddIntroVideoPath)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response AddTrainigVideo(@FormParam(VideoWS.VideoTitle) String VideoTitle, @FormParam(VideoWS.VideoPath) String VideoPath,
            @FormParam(VideoWS.VideoDescription) String VideoDescription, @FormParam(VideoWS.VideoType) String VideoType)
            throws Exception {

        MongoClient mongoClient = new MongoClient(HOST, PORTNO);

        DB db = mongoClient.getDB(DATABASE);
        DBCollection videos = db.getCollection(INTROCOLLECTION);
        try {
            BasicDBObject keys = new BasicDBObject("VideoTitle", 1);
            BasicDBObject options = new BasicDBObject(UNIQUE, TRUE);
            videos.createIndex(keys, options);

            System.out.println(VideoPath);
            System.out.println(VideoTitle);
            System.out.println(VideoDescription);
            System.out.println(VideoType);
            ;
            BasicDBObject obj = new BasicDBObject();

            obj.append("VideoTitle", VideoTitle);
            obj.append("VideoPath", VideoPath);
            obj.append("VideoDescription", VideoDescription);
            obj.append("VideoCategory", VideoCategory);
            obj.append("VideoType", VideoType);

            WriteResult result = videos.insert(obj);

            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("VideoTitle", VideoTitle);
            jsonObject.put("VideoPath", VideoPath);
            jsonObject.put("VideoDescription", VideoDescription);
            jsonObject.put("VideoType", VideoType);

            jsonObject.put("Message", VideoSuccess);
            array.put(jsonObject);
            String Success = array.toString();
            return Response.status(200).entity(Success).build();
        } catch (Exception e) {
            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("VideoTitle", VideoTitle);
            jsonObject.put("VideoPath", VideoPath);
            jsonObject.put("VideoDescription", VideoDescription);
            jsonObject.put("VideoType", VideoType);

            jsonObject.put("Message", VideoFailed);
            array.put(jsonObject);
            String Fail = array.toString();
            return Response.status(400).entity(Fail).build();

        }
    }

    @GET
    @Path(GetAllTrainigVideoPath)
    @Produces(MediaType.APPLICATION_JSON)
    public String geTrainingVideos() throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(TRAININGCOLLECTION);
        DBCursor cursor = coll.find().sort(new BasicDBObject("by", 1));
        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();

                list.add(o.toString());

            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

    @GET
    @Path(GetAllIntroVideo)
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllIntroVideos() throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(INTROCOLLECTION);
        DBCursor cursor = coll.find().sort(new BasicDBObject("by", 1));
        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();

                list.add(o.toString());

            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

    @GET
    @Path(GetTrainigVideo)
    @Produces(MediaType.APPLICATION_JSON)
    public String getTrainigVideo(@QueryParam(VideoWS.VideoTitle) String VideoTitle) throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(TRAININGCOLLECTION);
        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put("VideoTitle", VideoTitle);
        DBCursor cursor = coll.find(whereQuery);
        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                list.add(o.toString());
            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

    @GET
    @Path(GetIntroVideo)
    @Produces(MediaType.APPLICATION_JSON)
    public String getIntroVideo(@QueryParam(VideoWS.VideoTitle) String VideoTitle) throws UnknownHostException {
        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        DB db = mongoClient.getDB(DATABASE);
        DBCollection coll = db.getCollection(INTROCOLLECTION);
        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put("VideoTitle", VideoTitle);
        DBCursor cursor = coll.find(whereQuery);
        List<String> list = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                DBObject o = cursor.next();
                list.add(o.toString());
            }
        } finally {
            cursor.close();
            mongoClient.close();
        }
        return list.toString();
    }

    @POST
    @Path(UpdateTrainingVideoInfo)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response updateTraingVideo(@FormParam(VideoWS.VideoTitle) String VideoTitle, @FormParam(VideoWS.VideoPath) String VideoPath,
            @FormParam(VideoWS.VideoDescription) String VideoDescription, @FormParam(VideoWS.VideoCategory) String VideoCategory,
            @FormParam(VideoWS.VideoType) String VideoType) throws Exception {

        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);

        DBCollection register = db.getCollection(TRAININGCOLLECTION);
        DBObject query = new BasicDBObject("VideoTitle", VideoTitle);
        DBObject update = new BasicDBObject();
        try {

            update.put(SET, new BasicDBObject("VideoType", VideoType));
            update.put(SET, new BasicDBObject("VideoPath", VideoPath));
            update.put(SET, new BasicDBObject("VideoDescription", VideoDescription));
            update.put(SET, new BasicDBObject("VideoCategory", VideoCategory));

            WriteResult result = register.update(query, update);
            mongoClient.close();
            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("VideoTitle", VideoTitle);
            jsonObject.put("VideoPath", VideoPath);
            jsonObject.put("VideoDescription", VideoDescription);
            jsonObject.put("VideoType", VideoType);
            jsonObject.put("Message", UpdateSuccess);
            array.put(jsonObject);
            String Success = array.toString();
            return Response.status(200).entity(Success).build();

        } catch (Exception e) {
            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("VideoTitle", VideoTitle);
            jsonObj.put("VideoPath", VideoPath);
            jsonObj.put("VideoDescription", VideoDescription);
            jsonObj.put("VideoType", VideoType);
            jsonObj.put("Message", UpdateFail);
            arr.put(jsonObj);
            String Success = arr.toString();
            return Response.status(200).entity(Success).build();

        } finally {
            mongoClient.close();
        }

    }

    @POST
    @Path(UpdateIntroVideoInfo)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    public Response updateIntroVideos(@FormParam(VideoWS.VideoTitle) String VideoTitle, @FormParam(VideoWS.VideoPath) String VideoPath,
            @FormParam(VideoWS.VideoDescription) String VideoDescription, @FormParam(VideoWS.VideoCategory) String VideoCategory,
            @FormParam(VideoWS.VideoType) String VideoType) throws Exception {

        MongoClient mongoClient = new MongoClient(HOST, PORTNO);
        // Now connect to your databases
        DB db = mongoClient.getDB(DATABASE);

        DBCollection register = db.getCollection(INTROCOLLECTION);
        DBObject query = new BasicDBObject("VideoTitle", VideoTitle);
        DBObject update = new BasicDBObject();
        try {

            update.put(SET, new BasicDBObject("VideoType", VideoType));
            update.put(SET, new BasicDBObject("VideoPath", VideoPath));
            update.put(SET, new BasicDBObject("VideoDescription", VideoDescription));
            update.put(SET, new BasicDBObject("VideoCategory", VideoCategory));

            WriteResult result = register.update(query, update);
            mongoClient.close();
            JSONArray array = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("VideoTitle", VideoTitle);
            jsonObject.put("VideoPath", VideoPath);
            jsonObject.put("VideoDescription", VideoDescription);
            jsonObject.put("VideoType", VideoType);
            jsonObject.put("Message", UpdateSuccess);
            array.put(jsonObject);
            String Success = array.toString();
            return Response.status(200).entity(Success).build();

        } catch (Exception e) {
            JSONArray arr = new JSONArray();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("VideoTitle", VideoTitle);
            jsonObj.put("VideoPath", VideoPath);
            jsonObj.put("VideoDescription", VideoDescription);
            jsonObj.put("VideoType", VideoType);
            jsonObj.put("Message", UpdateFail);
            arr.put(jsonObj);
            String Success = arr.toString();
            return Response.status(200).entity(Success).build();

        } finally {
            mongoClient.close();
        }

    }

}
