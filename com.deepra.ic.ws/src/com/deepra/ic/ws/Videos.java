package com.deepra.ic.ws;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Path("/videos")
public class Videos {

    @Path("intro")
    @GET
    @Produces("application/json")
    public Response intro() throws JSONException {

        JSONArray array = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "Connexions PROMO");
        jsonObject.put("path", "https://youtu.be/2B1wkAmxZWU");
        jsonObject.put("description", "Connexions the TV series - Short promo");
        array.put(jsonObject);

        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("title", "Ep 1 – Making The Connexions");
        jsonObject1.put("path", "https://youtu.be/WJ-LyGDSNwI?list=PL_c3OUAscMYV15JTls0EjvDVmjYp7I-GL");
        jsonObject1.put("description", "Dr David J Hall's Connexions - Ep 1");
        array.put(jsonObject1);

        String result = array.toString();
        return Response.status(200).entity(result).build();
    }

    @Path("trainingLib")
    @GET
    @Produces("application/json")
    public Response trainingLib() throws JSONException {

        JSONArray array = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "Connexions PROMO");
        jsonObject.put("path", "https://youtu.be/2B1wkAmxZWU");
        jsonObject.put("description", "Connexions the TV series - Short promo");
        array.put(jsonObject);

        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("title", "Ep 1 – Making The Connexions");
        jsonObject1.put("path", "https://youtu.be/WJ-LyGDSNwI?list=PL_c3OUAscMYV15JTls0EjvDVmjYp7I-GL");
        jsonObject1.put("description", "Dr David J Hall's Connexions - Ep 1");
        array.put(jsonObject1);

        String result = array.toString();
        return Response.status(200).entity(result).build();
    }

    @Path("techniquesVideoscribes")
    @GET
    @Produces("application/json")
    public Response techniquesVideoscribes() throws JSONException {

        JSONArray array = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "Synectics – Barbell");
        jsonObject.put("path",
                "https://r5---sn-ci5gup-h55l.googlevideo.com/videoplayback?upn=OR4L5_1NMB0&ratebypass=yes&signature=0329C6D47CBD52BC0C55BF26CFD890297BB3A69E.46A074376EC48F789FA9B42877214628E58EDFDD&source=youtube&key=yt6&sver=3&expire=1466166082&initcwndbps=557500&dur=177.168&id=o-AMoMQiqf10iIqfmGBiXsFYxLnCpPnLJgZRT9JOz7O9SZ&mime=video%2Fmp4&ms=au&requiressl=yes&pl=19&itag=18&mv=m&sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&mn=sn-ci5gup-h55l&mm=31&ip=122.171.74.11&mt=1466144218&lmt=1366067008801764&fexp=9405348%2C9408207%2C9410705%2C9416126%2C9416891%2C9419452%2C9422596%2C9428398%2C9431012%2C9431684%2C9432369%2C9432684%2C9433096%2C9433239%2C9433380%2C9433656%2C9433946%2C9435526%2C9435876%2C9436026%2C9436390%2C9436588%2C9436874%2C9437066%2C9437473%2C9437553%2C9438326%2C9438336%2C9438567%2C9438661%2C9439653&ipbits=0&title=Synectics+-+Barbell%20%5B360p%5D");
        jsonObject.put("description", "Synectics – Barbell");
        array.put(jsonObject);

        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("title", "Bionics – Magnetism");
        jsonObject1.put("path",
                "https://r1---sn-ci5gup-h55l.googlevideo.com/videoplayback?sparams=dur%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&signature=C8A911D67B45553A95F12BB6BD62DEBD6616CFC0.58C9092230164D88A727C8CC0C3E41638147B603&sver=3&upn=ibRJaAklmoM&itag=18&key=yt6&mime=video%2Fmp4&requiressl=yes&initcwndbps=557500&dur=300.141&source=youtube&lmt=1366672817589131&ratebypass=yes&fexp=9405348%2C9408207%2C9410705%2C9416126%2C9416891%2C9419452%2C9422596%2C9428398%2C9431012%2C9431684%2C9432369%2C9432684%2C9433096%2C9433239%2C9433380%2C9433656%2C9433946%2C9435526%2C9435876%2C9436026%2C9436390%2C9436588%2C9436874%2C9437066%2C9437473%2C9437553%2C9438326%2C9438336%2C9438567%2C9438661%2C9439653&expire=1466166082&id=o-AHAr5jsywFt1TznCqSg2PfKP4uxqa3V-5ud-WqfNSJBf&mn=sn-ci5gup-h55l&mm=31&mt=1466144218&ip=122.171.74.11&ms=au&mv=m&pl=19&ipbits=0&title=Bionics+-+Magnetism%20%5B360p%5D");
        jsonObject1.put("description", "Bionics – Magnetism");
        array.put(jsonObject1);

        String result = array.toString();
        return Response.status(200).entity(result).build();
    }

    @Path("caseStudy")
    @GET
    @Produces("application/json")
    public Response caseStudy() throws JSONException {

        JSONArray caseArr = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", "Man Overboard!");
        jsonObject.put("subtitle", "Case Study - demonstrating how Lego is used as a creative problem solving tool");
        jsonObject.put("key", "Generation of ideas which are both novel and useful");
        jsonObject.put("introtext",
                "This Case Study was generated at an Ideas Centre workshop in Poole (UK).  The “Problem Owner” was the Innovation Director at a lifeboat service provider, who had been battling for some time to solve the problem of relatively low location rates for individuals who were reported to be overboard at sea.The whole problem solving process took 30 minutes – involving just 2 people.");
        jsonObject.put("problem", "How can we locate people who are overboard at sea more effectively?");
        jsonObject.put("introconclusion",
                "Whatever solution is generated in Lego – assuming that the model does indeed represent the problem absolutely solved – is simply a metaphor for a solution in the real world; the challenge (best generated with a skilled Facilitator) is to translate the metaphor!");
        jsonObject.put("techniqueUsed", "Lego");
        jsonObject.put("problemtext",
                "	Lego is a powerful creative problem solving tool, promoted commercially by Lego themselves (see www.seriousplay.com).  Indeed, one can become accredited by Lego as a qualified Lego-problem-solver!The technique is based on engagement with play, and accessing the more novel approaches within the right brain. Ingrained patterns of understanding of the problem at hand are scrambled by a requirement to model the issue using Lego - disorienting the problem owner and stimulating new ways of looking at the issue.  Even the most abstract of problems can be effectively modelled – then providing a 3-dimensional, tangible, model of the issue, that can be pushed and probed by a Facilitator to make sure that the model is as good as it can be.  It is then possible to literally “see” the issue.Once the problem is modelled, one then stays in “Lego play” mode and solves the problem – based on the model that has just been produced.  An independent, objective, naïve, Facilitator is helpful here – ensuring that the solution is derived from the Lego model, rather than force-fitting a real-world solution into the model.");	
		
		JSONArray videoArr = new JSONArray();
    	JSONObject videoObject = new JSONObject();
    			videoObject.put("introVideoTitle", "IntroVideo");
    			videoObject.put("introVideoDesc", "Intro Video Short Description");
		    	videoObject.put("introVideo", "https://youtu.be/2B1wkAmxZWU");
		    	videoObject.put("introVideoText", "Whatever solution is generated in Lego – assuming that the model does indeed represent the problem absolutely solved – is simply a metaphor for a solution in the real world; the challenge (best generated with a skilled Facilitator) is to translate the metaphor!");
		    	videoArr.put(videoObject);
		    	jsonObject.put("introVideoList", videoArr);
		    	
		   
		
		
		JSONArray stepsArr = new JSONArray();
    	JSONObject jsonObject2 = new JSONObject();
    			jsonObject2.put("name","Step-1");
    			jsonObject2.put("desc","represent the issue via Lego:");
    			jsonObject2.put("text","Here is the model produced by the Problem Owner:(a) Blue bricks represent the waves at sea.  The yellow brick in the right of picture (arrowed) represents the reported location of the overboard person.  The white bricks indicate the computer-estimated search area (based on wind direction and tides).  No joy – the computer models frequently get it wrong so that the boat is searching in the wrong place.  The person might be quite close, but is actually behind the “wave” on the far left (but cannot be seen – hidden by the wave – arrowed in (b)).  It was interesting to earn that it only take a 6 inch wave to obscure the man from the boat.  Scary!");
    			
    			JSONArray videoArr2 = new JSONArray();
    	    	JSONObject subjsonObject2 = new JSONObject();
    	    				subjsonObject2.put("videoTitle","Video1");
    	    				subjsonObject2.put("video","https://youtu.be/2B1wkAmxZWU");
    	    				subjsonObject2.put("videoText","However, clearly, not everyone would buy such clothing.  Hmmm.");
    	    				videoArr2.put(subjsonObject2);
    	    	jsonObject2.put("videoList",videoArr2 );
    	    	stepsArr.put(jsonObject2);
    	    	
    			JSONObject jsonObject3 = new JSONObject();
    				jsonObject3.put("name","Step-2");
    				jsonObject3.put("desc"," solve the problem in Legoland!");
    				
    				JSONArray videoArr3 = new JSONArray();
        	    	JSONObject subjsonObject3 = new JSONObject();
        	    			subjsonObject3.put("videoTitle","Video2");
		        	    	subjsonObject3.put("video","https://youtu.be/2B1wkAmxZWU");
		        	    	subjsonObject3.put("videoText","Now, having modelled the problem, we can use the same model (playing in Legoland) to solve the problem.  No need to question the logic, and don’t worry about reality – simply ask … “what do we have to do to this model to solve the problem”?  It is often easier for the naïve Facilitator to generate a solution, as we know that they are not burdened in any way by the reality of the situation, so it is much easier to solve the problem based in this imaginary world of Lego.  The Problem Owner will all too often try base their solutions on what has been tried before (i.e. reality), and then reverse this into the Lego model – then losing the creative magic of the process.In this Case Study, after very little thought, the “play” solution was easy – simply remove the blue brick, revealing the man-overboard to the boat by creating a direct line of sight (c). ");
		        	    	videoArr3.put(subjsonObject3);
		        	jsonObject3.put("videoList",videoArr3 );
        	    	stepsArr.put(jsonObject3);
        	    	
    	jsonObject.put("Steps",stepsArr );
    	caseArr.put(jsonObject);
    				
		
		
		String result = caseArr.toString();
		return Response.status(200).entity(result).build();
	  }
	  
}
