/*
 * package com.deepra.ic.ws;
 * 
 * import java.io.IOException; import java.security.Principal; import
 * javax.annotation.Priority; import javax.servlet.http.Cookie; import
 * javax.servlet.http.HttpServletRequest; import javax.ws.rs.Priorities; import
 * javax.ws.rs.container.ContainerRequestContext; import
 * javax.ws.rs.container.ContainerRequestFilter; import
 * javax.ws.rs.core.Context; import javax.ws.rs.core.Response; import
 * javax.ws.rs.core.SecurityContext; import javax.ws.rs.ext.Provider; import
 * com.mongodb.BasicDBObject; import com.mongodb.DB; import
 * com.mongodb.DBCollection; import com.mongodb.DBObject; import
 * com.mongodb.MongoClient; import
 * com.deepra.ic.ws.WebRegistration.Authenticated;
 * 
 * 
 * 
 * @Provider
 * 
 * @Authenticated
 * 
 * @Priority(Priorities.AUTHORIZATION) public class AuthorizationFilterWS
 * implements SecurityContext, ContainerRequestFilter {
 * 
 * @Context private HttpServletRequest request;
 * 
 * private static final String COLLECTION_REGISTER = "ideascentregroup"; public
 * static final String HOST="localhost"; private static final String
 * TOKEN="token"; private static final String TOKEN_COOKIE="tokenCookie";
 * private static final String COOKIE_NULL_MSG="Cookie null"; public static
 * final String DATABASE="AdminRegister"; public static final String DB_VALUE=
 * "DB: "; public static final String COOKIE_VALUE="Cookie: ";
 * 
 * 
 * 
 * @Override public void filter(ContainerRequestContext requestContext ) throws
 * IOException {
 * 
 * String token=""; String DBToken=""; Cookie[] cookies = request.getCookies();
 * 
 * try{ if (cookies == null) { System.out.println(COOKIE_NULL_MSG);
 * requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build(
 * )); } }catch (Exception e) {
 * requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build(
 * )); } try{ for (Cookie cookie : cookies) {
 * 
 * if (cookie.getName().equals(TOKEN_COOKIE)) { token=cookie.getValue();
 * 
 * MongoClient mongoClient = new MongoClient(HOST, 27017); // Now connect to
 * your databases DB db = mongoClient.getDB(DATABASE); DBCollection register =
 * db.getCollection(COLLECTION_REGISTER); DBObject query = new
 * BasicDBObject(TOKEN, token); // resultant document fetched by satisfying the
 * criteria DBObject dbo = register.findOne(query); DBToken =
 * dbo.get(TOKEN).toString(); System.out.println(DB_VALUE +DBToken);
 * System.out.println(COOKIE_VALUE+cookie.getValue());
 * if(DBToken.equals(token)){ return; } mongoClient.close();
 * 
 * } } }catch (Exception e) {
 * requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build(
 * )); }
 * 
 * requestContext.setSecurityContext(new SecurityContext() { SecurityContext
 * securityContext;
 * 
 * @Override public Principal getUserPrincipal() {
 * 
 * return new Principal() {
 * 
 * @Override public String getName() { String email=""; return email; } }; }
 * 
 * @Override public boolean isUserInRole(String role) { // TODO Auto-generated
 * method stub return false; }
 * 
 * @Override public boolean isSecure() { // TODO Auto-generated method stub
 * return false; }
 * 
 * @Override public String getAuthenticationScheme() { // TODO Auto-generated
 * method stub return null; }
 * 
 * }); }
 * 
 * @Override public Principal getUserPrincipal() { // TODO Auto-generated method
 * stub return null; }
 * 
 * @Override public boolean isUserInRole(String role) { // TODO Auto-generated
 * method stub return false; }
 * 
 * @Override public boolean isSecure() { // TODO Auto-generated method stub
 * return false; }
 * 
 * @Override public String getAuthenticationScheme() { // TODO Auto-generated
 * method stub return null; }
 * 
 * 
 * 
 * }
 */