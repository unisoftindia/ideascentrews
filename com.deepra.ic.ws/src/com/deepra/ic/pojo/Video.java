package com.deepra.ic.pojo;

import javax.xml.bind.annotation.XmlElement;

public class Video {
    private String VideoId;
    private String VideoPath;
    private String VideoTitle;
    private String VideoDescription;
    private String VideoType;

    @XmlElement
    public String getVideoPath() {
        return VideoPath;
    }

    public void setVideoPath(String videoPath) {
        VideoPath = videoPath;
    }

    @XmlElement
    public String getVideoTitle() {
        return VideoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        VideoTitle = videoTitle;
    }

    @XmlElement
    public String getVideoDescription() {
        return VideoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        VideoDescription = videoDescription;
    }

    @XmlElement
    public String getVideoType() {
        return VideoType;
    }

    public void setVideoType(String videoType) {
        VideoType = videoType;
    }

    @XmlElement
    public String getVideoId() {
        return VideoId;
    }

    public void setVideoId(String videoId) {
        VideoId = videoId;
    }
}
